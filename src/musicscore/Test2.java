package musicscore;

import javax.swing.*;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Test2 {

    public static void main(String[] args) throws IOException, FontFormatException, InterruptedException {

        JFrame jFrame = new JFrame("五线谱绘制测试");

        jFrame.setBounds(0, 0, 1600, 1000);


        InputStream inputStream = new FileInputStream("C:\\Users\\Administrator\\IdeaProjects\\musicsocre\\src\\old.test\\akvo.ttf");


        jFrame.setVisible(true);

        int size = 60;
        Graphics pen = jFrame.getContentPane().getGraphics();
        System.out.println(pen);
        Font font1 = Font.createFont(Font.PLAIN, inputStream);
        font1 = font1.deriveFont(java.awt.Font.PLAIN, size);
        GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font1);
        pen.setFont(font1);

        while (true) {

            pen.drawString(String.valueOf(')'), size, 100);

            for (int j = 1; j <= 10; j = j + 1) {


                pen.drawString(String.valueOf('\''), j * size, 100);

            }


            Thread.sleep(100);
        }

    }
}

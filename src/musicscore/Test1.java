package musicscore;

import javax.swing.*;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

//测试基本符号
public class Test1 {

    public static void main(String[] args) throws IOException, FontFormatException, InterruptedException {

        JFrame jFrame = new JFrame("五线谱绘制测试");

        jFrame.setBounds(0, 0, 1600, 1000);


        InputStream inputStream = new FileInputStream("C:\\Users\\Administrator\\IdeaProjects\\musicsocre\\src\\old.test\\akvo.ttf");


        jFrame.setVisible(true);

        Graphics pen = jFrame.getContentPane().getGraphics();
        System.out.println(pen);
        Font font1 = Font.createFont(Font.PLAIN, inputStream);
        font1 = font1.deriveFont(java.awt.Font.PLAIN, 60);
        GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font1);


        while (true) {
            int k = 1;
            for (int i = 1; i <= 50; i = i + 1) {
                for (int j = 1; j <= 10; j = j + 1) {

                    pen.setFont(font1);
                    char q = (char) k;
                    pen.drawString(String.valueOf(q), i * 80, j * 80 + 20);
                    k = k + 1;

                    Font font2 = new Font("songti", 1, 10);
                    pen.setFont(font2);
                    pen.drawString(String.valueOf((char) k), i * 80, j * 80 + 20 + 15);
                }
            }


            Thread.sleep(100);
        }

    }
}

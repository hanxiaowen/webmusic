package midi2;
/**
 * Midi文件
 * byte范围：-128~127
 * midi格式：4d 54 68 64 00 00 00 06 ff ff nn nn dd dd
 * ff ff轨道:00 00 单轨道
 *          00 01 同步多轨道
 *          00 02 不同步多轨道
 * nn nn 轨道个数，为实际轨道个数+1个全局音轨
 * dd dd 基本时间类型
 *
 * 同步多轨道：00 01
 * 1个轨道：00 01
 * 120为四分音符：00 78
 */
public class MidiFile {

    //头部
    byte head[] = {0, 0, 0, 0};

    //头部长度
    byte headLen[] = {0, 0, 0, 6};

    //轨道类型：0单轨道 1多轨道同步 2多轨道不同步
    int trackType;

    //轨道个数
    int trackNum;

    //基本间隔时间
    int tickTime;
}

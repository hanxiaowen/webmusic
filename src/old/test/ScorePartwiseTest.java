package old.test;

import old.scorePartwise.ScorePartwise;
import old.scorePartwise.Work;
import old.scorePartwise.part.Part;
import old.scorePartwise.part.measure.Measure;
import old.scorePartwise.part.measure.attributes.Clef;
import old.scorePartwise.part.measure.note.Note;
import old.scorePartwise.part.measure.note.Pitch;
import old.scorePartwise.partList.scorePart.ScorePart;
import old.scorePartwise.partList.scorePart.partName.PartName;

public class ScorePartwiseTest {
    public static void main(String[] args) {

        //乐谱
        ScorePartwise scorePartwise = new ScorePartwise(new Work("测试乐谱"));

        //分谱
        scorePartwise.addScorePart(new ScorePart("part1", new PartName("Piano")));

        //内容
        Part part = new Part("part1");

        //小节
        Measure measure = new Measure();

        //小节谱号，一个高音谱号，一个低音谱号
        measure.getAttributes().addClef(new Clef(Clef.SIGN_G, Clef.LINE_G, 1));
        measure.getAttributes().addClef(new Clef(Clef.SIGN_F, Clef.LINE_F, 2));


        //增加两个音符，一个在谱1，一个在谱2
        Note note1 = new Note(new Pitch(Pitch.STEP_C, 0, 5), Note.TYPE_whole, 1);
        Note note2 = new Note(new Pitch(Pitch.STEP_C, 0, 4), Note.TYPE_whole, 2);
        measure.addNote(note1);
        measure.addNote(note2);


        part.addMeasure(measure);
        scorePartwise.addPart(part);

        scorePartwise.toMusicXML("测试1.musicxml");




    }
}

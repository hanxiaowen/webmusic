package old.test;


import old.scorePartwise.ScorePartwise;
import old.scorePartwise.Work;
import old.scorePartwise.part.Part;
import old.scorePartwise.part.measure.Measure;
import old.scorePartwise.part.measure.attributes.Clef;

import old.scorePartwise.part.measure.note.Note;
import old.scorePartwise.part.measure.note.Pitch;
import old.scorePartwise.partList.scorePart.ScorePart;
import old.scorePartwise.partList.scorePart.partName.PartName;

public class 小星星 {
    public static void main(String[] args) {

        //乐谱
        ScorePartwise scorePartwise = new ScorePartwise(new Work("测试乐谱"));

        //分谱
        scorePartwise.addScorePart(new ScorePart("part1", new PartName("Piano")));

        //内容
        Part part = new Part("part1");

        //小节
        Measure measure = new Measure();
        measure.getAttributes().setStaves(2);

        //小节谱号，一个高音谱号，一个低音谱号
        measure.getAttributes().addClef(new Clef(Clef.SIGN_G, Clef.LINE_G, 1));
        measure.getAttributes().addClef(new Clef(Clef.SIGN_F, Clef.LINE_F, 2));


        //增加两个音符，一个在谱1，一个在谱2
        measure.addNote(new Note(new Pitch(Pitch.STEP_C, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_C, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_G, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_G, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_A, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_A, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_G, 0, 5), Note.TYPE_half, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_F, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_F, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_E, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_E, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_D, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_D, 0, 5), Note.TYPE_quarter, 1));
        measure.addNote(new Note(new Pitch(Pitch.STEP_C, 0, 5), Note.TYPE_half, 1));



        measure.addNote(new Note(new Pitch(Pitch.STEP_C, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_G, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_E, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_G, 0, 4), Note.TYPE_eighth, 2));

        measure.addNote(new Note(new Pitch(Pitch.STEP_C, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_G, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_E, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_G, 0, 4), Note.TYPE_eighth, 2));

        measure.addNote(new Note(new Pitch(Pitch.STEP_C, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_A, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_F, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_A, 0, 4), Note.TYPE_eighth, 2));

        measure.addNote(new Note(new Pitch(Pitch.STEP_C, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_G, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_E, 0, 4), Note.TYPE_eighth, 2));
        measure.addNote(new Note(new Pitch(Pitch.STEP_G, 0, 4), Note.TYPE_eighth, 2));


        part.addMeasure(measure);
        scorePartwise.addPart(part);

        scorePartwise.toMusicXML("小星星.musicxml");


    }
}

package old.scorePartwise.part.measure;

import old.scorePartwise.part.measure.attributes.Attributes;
import old.scorePartwise.part.measure.direction.Direction;
import old.scorePartwise.part.measure.note.Note;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
//小节
public class Measure {

    //小节编号,默认从1开始
    @XmlAttribute
    int number = 1;

    //小节属性
    @XmlElement
    Attributes attributes = new Attributes();

    //方向信息
    Direction direction = null;

    //音符列表
    @XmlElement(name = "note")
    List<Note> noteList = new ArrayList<>();

    //增加音符
    public void addNote(Note note){
        this.noteList.add(note);
    }


    public Measure(int number) {
        this.number = number;
    }

    public Measure() {
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public List<Note> getNoteList() {
        return noteList;
    }

    public void setNoteList(List<Note> noteList) {
        this.noteList = noteList;
    }
}

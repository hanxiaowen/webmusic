package old.scorePartwise.part.measure.attributes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Time {

    //节奏，默认44拍
    @XmlElement
    int beats=4;

    //节奏类型
    @XmlElement(name = "beat-type")
    int beatType=4;

    public Time() {
    }

    public Time(int beats, int beatType) {
        this.beats = beats;
        this.beatType = beatType;
    }

    public int getBeats() {
        return beats;
    }

    public void setBeats(int beats) {
        this.beats = beats;
    }

    public int getBeatType() {
        return beatType;
    }

    public void setBeatType(int beatType) {
        this.beatType = beatType;
    }
}

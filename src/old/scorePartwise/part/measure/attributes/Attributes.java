package old.scorePartwise.part.measure.attributes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

//小节属性
@XmlAccessorType(XmlAccessType.FIELD)
public class Attributes {

    //四份音符时间值,默认为256
    @XmlElement(name = "divisions")
    int division = 256;


    //调号，升降调个数，0为C调
    @XmlElement(name = "key")
    Key key = new Key(0);

    //节奏，默认为4/4拍
    @XmlElement(name = "time")
    Time time = new Time(4, 4);

    //多五线谱,谱子并列个数，高音谱和低音谱的个数
    @XmlElement(name = "staves")
    int staves = 1;


    //谱号列表：低音谱F、中音谱C、高音谱G
    @XmlElement(name = "clef")
    List<Clef> clefList = new ArrayList() {{
        //add(new Clef(Clef.SIGN_G, Clef.LINE_G, 1));
    }};

    public int getStaves() {
        return staves;
    }

    public void setStaves(int staves) {
        this.staves = staves;
    }

    public Attributes() {
    }

    public void addClef(Clef clef) {
        this.clefList.add(clef);
    }

    public Attributes(int division, Key key, Time time) {
        this.division = division;
        this.key = key;
        this.time = time;
    }

    public int getDivision() {
        return division;
    }

    public void setDivision(int division) {
        this.division = division;
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public List<Clef> getClefList() {
        return clefList;
    }

    public void setClefList(List<Clef> clefList) {
        this.clefList = clefList;
    }
}

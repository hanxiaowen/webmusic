package old.scorePartwise.part.measure.attributes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

//谱号
@XmlAccessorType(XmlAccessType.FIELD)
public class Clef {

    //谱号：低音谱F、中音谱C、高音谱G
    @XmlElement
    String sign = "G";

    //线高
    @XmlElement
    int line = 2;

    //谱编号
    @XmlAttribute
    int number = 1;

    //低音、中音、高音 谱表 常量
    public static String SIGN_G = "G";
    public static String SIGN_F = "F";
    public static String SIGN_C = "C";
    public static int LINE_G = 2;
    public static int LINE_F = 4;
    public static int LINE_C = 4;

    public Clef() {
    }

    public Clef(String sign, int line, int number) {
        this.sign = sign;
        this.line = line;
        this.number = number;
    }

    public Clef(String sign, int number) {
        this.sign = sign;
        if (sign.equals(SIGN_G)) line = 2;
        if (sign.equals(SIGN_F)) line = 4;
        if (sign.equals(SIGN_C)) line = 4;
        this.number = number;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }
}

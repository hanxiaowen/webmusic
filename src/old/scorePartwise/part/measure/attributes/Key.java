package old.scorePartwise.part.measure.attributes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

//调号
@XmlAccessorType(XmlAccessType.FIELD)
public class Key {

    //升降调个数，C调为0
    @XmlElement
    int fifths = 0;

    //调式:major大调式，minor  小调式
    String mode = "major";

    public static String KEY_MAJOR = "major";
    public static String KEY_MINOR = "minor";

    public Key() {
    }

    public Key(int fifths) {
        this.fifths = fifths;
    }
}

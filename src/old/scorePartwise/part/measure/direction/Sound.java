package old.scorePartwise.part.measure.direction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

//声音
@XmlAccessorType(XmlAccessType.FIELD)
public class Sound {

    //速度
    @XmlAttribute
    int tempo = 90;

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }
}

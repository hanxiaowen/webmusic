package old.scorePartwise.part.measure.direction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

//方向
@XmlAccessorType(XmlAccessType.FIELD)
public class Direction {


    //标记位置 above 上面，below下面
    @XmlAttribute
    String placement = "above";

    //方向标记
    @XmlElement(name = "direction-type")
    DirectionType directionType = new DirectionType();

    //谱位置编号
    @XmlElement
    int staff = 1;

    //声音
    @XmlElement
    Sound sound = null;

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

    public DirectionType getDirectionType() {
        return directionType;
    }

    public void setDirectionType(DirectionType directionType) {
        this.directionType = directionType;
    }

    public int getStaff() {
        return staff;
    }

    public void setStaff(int staff) {
        this.staff = staff;
    }

    public Sound getSound() {
        return sound;
    }

    public void setSound(Sound sound) {
        this.sound = sound;
    }
}

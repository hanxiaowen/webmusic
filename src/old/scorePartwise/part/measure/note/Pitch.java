package old.scorePartwise.part.measure.note;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

//音
@XmlAccessorType(XmlAccessType.FIELD)
public class Pitch {

    //音高
    @XmlElement
    String step = "C";

    //升高半音
    @XmlElement
    int alter = 0;

    //八度,默认是C5
    @XmlElement
    int octave = 5;


    public static String STEP_C = "C";
    public static String STEP_D = "D";
    public static String STEP_E = "E";
    public static String STEP_F = "F";
    public static String STEP_G = "G";
    public static String STEP_A = "A";
    public static String STEP_B = "B";

    public Pitch() {
    }

    public Pitch(String step, int alter, int octave) {
        this.step = step;
        this.alter = alter;
        this.octave = octave;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public int getOctave() {
        return octave;
    }

    public void setOctave(int octave) {
        this.octave = octave;
    }
}

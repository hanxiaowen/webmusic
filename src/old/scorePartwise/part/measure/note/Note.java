package old.scorePartwise.part.measure.note;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.HashMap;

//音符
@XmlAccessorType(XmlAccessType.FIELD)
public class Note {

    //音符，C4为小字1组
    @XmlElement
    Pitch pitch = new Pitch("C", 0, 4);

    //时值
    @XmlElement
    int duration = 1024;

    //类型： 音符类型: 256th、128th、64th、32nd、16th、eighth、quarter、half、whole、breve、long
    @XmlElement
    String type = "whole";

    public static String TYPE_256th = "256th";
    public static String TYPE_128th = "128th";
    public static String TYPE_64th = "64th";
    public static String TYPE_32nd = "32nd";
    public static String TYPE_16th = "16th";
    public static String TYPE_eighth = "eighth";
    public static String TYPE_quarter = "quarter";
    public static String TYPE_half = "half";
    public static String TYPE_whole = "whole";
    public static String TYPE_breve = "breve";
    public static String TYPE_long = "long";

    //时值对照表
    public static HashMap<String, Integer> h = new HashMap<String, Integer>() {{
        put("whole", 1024);
        put("half", 512);
        put("quarter", 256);
        put("eighth", 128);
        put("16th", 64);
        put("32nd", 32);
        put("64th", 16);

    }};


    //谱位置编号
    @XmlElement
    int staff = 1;

    //附点标记
    @XmlElement
    Dot dot = null;

    //和弦标记
    @XmlElement
    Chord chord = null;

    //倚音标记
    @XmlElement
    Grace grace = null;

    //休止符标记
    @XmlElement
    Rest rest = null;

    public Note() {
    }

    public Note(Pitch pitch, String type, int staff) {
        this.pitch = pitch;
        this.duration = Note.h.get(type);
        this.type = type;
        this.staff = staff;
    }

    public Pitch getPitch() {
        return pitch;
    }

    public void setPitch(Pitch pitch) {
        this.pitch = pitch;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

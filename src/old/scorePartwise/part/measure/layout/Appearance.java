package old.scorePartwise.part.measure.layout;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.List;

//五线谱外观和显示布局
@XmlAccessorType(XmlAccessType.FIELD)
public class Appearance {

    List<NoteSize> noteSizeList;
}

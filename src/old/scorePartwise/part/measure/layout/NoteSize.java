package old.scorePartwise.part.measure.layout;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

//音符尺寸
@XmlAccessorType(XmlAccessType.FIELD)
public class NoteSize {

    //grace 和 cue
    @XmlAttribute
    String type;


    @XmlElement
    int value;
}

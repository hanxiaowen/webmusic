package old.scorePartwise.part;

import old.scorePartwise.part.measure.Measure;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

//乐谱正文
@XmlAccessorType(XmlAccessType.FIELD)
public class Part {


    //分谱编号，默认为p1
    @XmlAttribute
    String id = "p1";

    //小节列表
    @XmlElement(name = "measure")
    List<Measure> measureList = new ArrayList<>();

    public Part(String id) {
        this.id = id;
    }

    public Part() {
    }

    public Part(String id, List<Measure> measureList) {
        this.id = id;
        this.measureList = measureList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Measure> getMeasureList() {
        return measureList;
    }

    public void addMeasure(Measure measure){
        this.measureList.add(measure);
    }
    public void setMeasureList(List<Measure> measureList) {
        this.measureList = measureList;
    }
}

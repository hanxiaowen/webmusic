package old.scorePartwise.io;

import old.scorePartwise.ScorePartwise;
import old.scorePartwise.Work;
import old.scorePartwise.part.Part;
import old.scorePartwise.part.measure.Measure;
import old.scorePartwise.part.measure.attributes.Attributes;
import old.scorePartwise.part.measure.direction.Direction;
import old.scorePartwise.part.measure.direction.Sound;
import old.scorePartwise.part.measure.note.Note;
import old.scorePartwise.partList.scorePart.ScorePart;
import old.scorePartwise.partList.scorePart.partName.PartName;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

//文件输出辅助工具
public class ScorePartwiseWriter {

    public static void toFile(ScorePartwise scorePartwise, String path) {


        try {
            // 利用jdk中自带的转换类实现
            JAXBContext context = JAXBContext.newInstance(scorePartwise.getClass());
            Marshaller marshaller = context.createMarshaller();
            // 格式化xml输出的格式
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            // 将对象转换成输出流形式的xml
            marshaller.marshal(scorePartwise, new File(path));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        ScorePartwise scorePartwise = new ScorePartwise();
        scorePartwise.setVersion("3.0");
        scorePartwise.setWork(new Work("测试"));


        List<ScorePart> scorePartList = new ArrayList<>();

        ScorePart scorePart1 = new ScorePart("p1");
        scorePart1.setPartName(new PartName("Piano"));

        ScorePart scorePart2 = new ScorePart("p2");
        scorePart2.setPartName(new PartName("Piano"));

        scorePartList.add(scorePart1);
        //scorePartList.add(scorePart2);


        scorePartwise.setScorePartList(scorePartList);

        //分谱内容
        Part part1 = new Part("p1");
        Part part2 = new Part("2");

        List<Part> partList = new ArrayList<>();
        partList.add(part1);
        //partList.add(part2);

        Measure measure = new Measure(1);
        List<Measure> measureList = new ArrayList<>();
        measure.getNoteList().add(new Note());
        measureList.add(measure);
        Attributes attributes = new Attributes();
        attributes.setDivision(256);

        Direction direction = new Direction();
        Sound sound = new Sound();
        sound.setTempo(120);
        direction.setSound(sound);
        measure.setDirection(direction);

        measure.setAttributes(attributes);
        part1.setMeasureList(measureList);
        scorePartwise.setPartsList(partList);

        toFile(scorePartwise, "测试3.musicxml");
    }
}

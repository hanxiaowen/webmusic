package old.scorePartwise;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

//乐谱工作空间
@XmlAccessorType(XmlAccessType.FIELD)
public class Work {

    @XmlElement(name = "work-title")
    String workTitle;

    public Work(String workTitle) {
        this.workTitle = workTitle;
    }

    public String getWorkTitle() {
        return workTitle;
    }

    public void setWorkTitle(String workTitle) {
        this.workTitle = workTitle;
    }
}

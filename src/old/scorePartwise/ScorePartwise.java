package old.scorePartwise;

import old.scorePartwise.io.ScorePartwiseWriter;
import old.scorePartwise.part.Part;
import old.scorePartwise.partList.scorePart.ScorePart;

import javax.xml.bind.annotation.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

//musicxml乐谱文件
@XmlRootElement(name = "score-partwise")
@XmlAccessorType(XmlAccessType.FIELD)
public class ScorePartwise {

    //版本
    @XmlAttribute(name = "version")
    String version = "3.1";

    //标题
    @XmlElement(name = "work")
    Work work = new Work("");

    //声部列表
    @XmlElementWrapper(name = "part-list")
    @XmlElement(name = "score-part")
    List<ScorePart> scorePartList = new ArrayList<>();

    //声部
    @XmlElement(name = "part")
    List<Part> partsList = new ArrayList<>();

    public ScorePartwise() {
    }

    public ScorePartwise(Work work) {
        this.work = work;
    }

    public List<ScorePart> getScorePartList() {
        return scorePartList;
    }

    public void setScorePartList(List<ScorePart> scorePartList) {
        this.scorePartList = scorePartList;
    }

    //增加分谱
    public void addScorePart(ScorePart scorePart) {
        this.scorePartList.add(scorePart);
    }

    //增加分谱内容
    public void addPart(Part part) {
        this.partsList.add(part);
    }

    //保存
    public void toMusicXML(String path) {
        ScorePartwiseWriter.toFile(this, path);
    }

    //加载
    public static ScorePartwise loadFromMusicXML(String path) {
        return null;
    }

    //从字符串加载
    public static ScorePartwise loadFromString(String string) {
        return null;
    }

    //播放
    public void play() {
    }

    //保存为五线谱或简谱
    public BufferedImage toBufferedImage() {
        return null;
    }

    //保存为midi文件
    public void toMidi(String path) {
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Work getWork() {
        return work;
    }

    public void setWork(Work work) {
        this.work = work;
    }


    public List<Part> getPartsList() {
        return partsList;
    }

    public void setPartsList(List<Part> partsList) {
        this.partsList = partsList;
    }
}

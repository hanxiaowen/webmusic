package old.scorePartwise.partList.scorePart;


import old.scorePartwise.partList.scorePart.partName.PartName;

import javax.xml.bind.annotation.*;

//分谱
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ScorePart {

    //分谱编号，默认为p1
    @XmlAttribute(name = "id")
    String id = "p1";

    //乐器名字，默认为钢琴
    @XmlElement(name = "part-name")
    PartName partName = new PartName("Piano");

    public ScorePart() {
    }

    public ScorePart(String id, PartName partName) {
        this.id = id;
        this.partName = partName;
    }

    public PartName getPartName() {
        return partName;
    }

    public void setPartName(PartName partName) {
        this.partName = partName;
    }

    public ScorePart(String id) {
        this.id = id;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}

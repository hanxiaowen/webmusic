package old.scorePartwise.partList.scorePart.partName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PartName {

    //乐器名字默认是 钢琴Piano
    @XmlValue
    String value = "Piano";

    public PartName(String value) {
        this.value = value;
    }

    public PartName() {
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

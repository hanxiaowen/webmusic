package old.scorePartwise.partList;

import old.scorePartwise.partList.scorePart.ScorePart;

import javax.xml.bind.annotation.*;
import java.util.List;

//分谱信息
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PartList {

    //分谱信息；列表
    @XmlElementWrapper(name = "score-part")
    @XmlElement(name = "score-part")
    List<ScorePart> scorePartList;

    public List<ScorePart> getScorePartList() {
        return scorePartList;
    }

    public void setScorePartList(List<ScorePart> scorePartList) {
        this.scorePartList = scorePartList;
    }
}

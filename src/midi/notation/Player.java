package midi.notation;

import midi.midi.MidiPlayer;
import midi.notation.util.BookUtil;


public class Player {
    public static void play(Book book) {
        String tempFile = System.currentTimeMillis() + "temp.mid";
        //保存到mid文件
        BookUtil.toMidiFile(book, tempFile);

        //播放
        MidiPlayer.play(tempFile);
    }

    public static void play(String fileName) {
        String tempFile = System.currentTimeMillis() + "temp.mid";
        Book book = BookUtil.fromPianoFile(tempFile);
        //保存到mid文件
        BookUtil.toMidiFile(book, tempFile);
        //播放
        MidiPlayer.play(tempFile);
    }
}

package midi.notation;

import midi.basic.Duration;
import midi.basic.Pitch;

import java.util.List;

//乐谱中的一个音符记号
/*
 *Book     Part Section Note
 *乐谱          声部  小节    音符
 */
public class Note {
    //音符列表
    public List<Pitch> pitchList;
    //休止记号
    public boolean isRestingNote;
    //长度
    public Duration duration;
    //力度
    public int velocity;
    //连奏开关,所有音符在这个长度时间内平均弹奏
    public boolean legato;
    //发音开关,如果为真就不弹奏
    public boolean off;
    //连接记号，表示弧括号的开始和结束
    public Connect connect;
    //琵琶音
    public boolean arpeggio;
    //颤音
    public boolean trill;
    //震音
    public boolean tremolo;
    //装饰音
    public List<Integer> ornament;

    //歌词
    public String text;

    public Note() {

    }


}

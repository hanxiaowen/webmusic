package midi.notation;

import midi.basic.Stave;

import java.util.ArrayList;
import java.util.List;

/**
 *Book     Part Section Note
 *乐谱          声部  小节    音符
 */
public class Part {

    //声部名称
    String partName;
    //谱表
    Stave stave;
    //所有小节
    List<Section> sectionList=new ArrayList<>();
    public void addSection(Section section){
        sectionList.add(section);
    }

    public String getPartName() {
        return partName;
    }

    public List<Section> getSectionList() {
        return sectionList;
    }

    public void setSectionList(List<Section> sectionList) {
        this.sectionList = sectionList;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }
}

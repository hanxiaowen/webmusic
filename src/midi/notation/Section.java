package midi.notation;

import midi.basic.Beat;
import midi.basic.Mode;
import midi.basic.Speed;

import java.util.ArrayList;
import java.util.List;

/**
 * 小节，每个小节包涵多个音符
 * Book     Part Section Note
 * 乐谱          声部  小节    音符
 */
public class Section {
    //小节的编号
    int id;
    //是否转调
    Boolean hasModeChanged;
    //转调的调式
    Mode mode;
    //是否变化节奏
    Boolean hasBeatChanged;
    //小节的节奏，一般情况下和乐谱的节奏相同，变节奏之后不同
    Beat beat;
    //是否改变速度
    Boolean hasSpeedChanged;
    Speed speed;
    //音符列表
    List<Note> noteList = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public Boolean getHasModeChanged() {
        return hasModeChanged;
    }

    public void setHasModeChanged(Boolean hasModeChanged) {
        this.hasModeChanged = hasModeChanged;
    }

    public List<Note> getNoteList() {
        return noteList;
    }

    public Boolean getHasBeatChanged() {
        return hasBeatChanged;
    }

    public void setHasBeatChanged(Boolean hasBeatChanged) {
        this.hasBeatChanged = hasBeatChanged;
    }

    public Beat getBeat() {
        return beat;
    }

    public void setBeat(Beat beat) {
        this.beat = beat;
    }

    public void setNoteList(List<Note> noteList) {
        this.noteList = noteList;
    }

    public Boolean getHasSpeedChanged() {
        return hasSpeedChanged;
    }

    public void setHasSpeedChanged(Boolean hasSpeedChanged) {
        this.hasSpeedChanged = hasSpeedChanged;
    }

    public Speed getSpeed() {
        return speed;
    }

    public void setSpeed(Speed speed) {
        this.speed = speed;
    }

    public Section() {
        hasBeatChanged=false;
        hasModeChanged=false;
        hasSpeedChanged=false;
    }

    public void addNote(Note... note) {
        for (int i = 0; i <= note.length - 1; i++) {
            noteList.add(note[i]);
        }
    }
}

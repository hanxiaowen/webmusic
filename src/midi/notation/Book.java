package midi.notation;

import midi.basic.Beat;
import midi.basic.Mode;
import midi.basic.Speed;

import java.util.ArrayList;
import java.util.List;

/**
 * 一个乐谱结构,对于一个piano文件
 * Book     Part Section Note
 * 乐谱          声部  小节    音符
 */
public class Book {
    //乐谱名称
    private String name;
    //乐谱日期
    private String dateTime;
    //乐谱作者
    private String auhtor;
    //乐谱编辑
    private String editor;
    //乐谱包括的调式
    private List<Mode> modeList=new ArrayList<>();
    //乐谱包括的节奏
    private List<Beat> beatList;
    //乐谱表情
    private Emotion emotion;
    //速度
    private List<Speed> speedList=new ArrayList<>();
    //说明
    private String desc;
    //声部
    private List<Part> partList = new ArrayList<>();

    public Book() {

    }

    public List<Beat> getBeatList() {
        return beatList;
    }

    public void setBeatList(List<Beat> beatList) {
        this.beatList = beatList;
    }

    public void addMode(Mode... mode) {
        for (Mode mode1 : mode) {
            this.modeList.add(mode1);

        }
    }

    public List<Mode> getModeList() {
        return modeList;
    }

    public void setModeList(List<Mode> modeList) {
        this.modeList = modeList;
    }

    public List<Part> getPartList() {
        return partList;
    }

    public void setPartList(List<Part> partList) {
        this.partList = partList;
    }

    public List<Speed> getSpeedList() {
        return speedList;
    }

    public void setSpeedList(List<Speed> speedList) {
        this.speedList = speedList;
    }

    public void addPart(Part... part) {
        for (Part p : part) {
            this.partList.add(p);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

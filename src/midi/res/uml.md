**1.midi包：硬件控制系统**
    用于控制MIDI系统并发出对应的端口发送指令
    
**2.notation包：电子记谱法**
    解析并记录piano格式的电子记谱法
    
    电子记谱法分为如下4个层次：
    Book乐谱
    Part声部
    Section小节
    Note音符记号
    

**3.theory：乐理与算法**

   音编号到音名记谱法和唱名记谱法直接的转换关系
    
   计算音程、和弦和转换调式
    
    中心法则
        音的唱名(Tone)表示-音编号(voiceId)-音的音名(Pitch)表示
    

package midi.test;

import midi.midi.IO.MidiFileIO;
import midi.midi.MThd.MThd;
import midi.midi.MThd.MidiType;
import midi.midi.MThd.TickTime;
import midi.midi.MTrk.MTrk;
import midi.midi.Message.Event.FinalEvent;
import midi.midi.Message.Event.KeyPressEvent;
import midi.midi.Message.Event.KeyRealseEvent;
import midi.midi.Message.Message;
import midi.midi.MidiFile;
import midi.midi.MidiPlayer;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Administrator on 2017/2/10.
 */
public class MidiPlayerTest {
    public static void main(String[] args) throws IOException {

        MidiFile midiFile = new MidiFile();
        //MThd头部
        MThd mThd = new MThd();
        mThd.tickTime = TickTime.Tick96;
        mThd.midiType = MidiType.Type00;
        mThd.MtrkNum = 1;
        midiFile.setmThd(mThd);

        //一条音轨
        MTrk mTrk = new MTrk();
        //按下一个键
        for (int i = 0; i <= 127; i++) {
            Message message = new Message();
            message.tick = mThd.tickTime.tickTimeValue();
            message.event = new KeyPressEvent(1, i, 100);
            mTrk.addMessage(message);
            if (i > 1) {
                Message message2 = new Message();
                message2.tick = mThd.tickTime.tickTimeValue();
                message2.event = new KeyRealseEvent(1, i - 1, 100);
                mTrk.addMessage(message2);
            }
        }
        Message message1 = new Message();
        message1.event = new FinalEvent();
        mTrk.addMessage(message1);

        midiFile.addMTrk(mTrk);
        //写出文件
        FileOutputStream fileOutputStream = new FileOutputStream("d:\\3.mid");
        MidiFileIO.write(fileOutputStream, midiFile);
        fileOutputStream.flush();
        fileOutputStream.close();

        MidiPlayer.play("d:\\3.mid");


    }
}

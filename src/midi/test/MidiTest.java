package midi.test;

import midi.midi.Common.MidiUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Administrator on 2017/2/10.
 */
public class MidiTest {
    //基本间隔时间，表示一个四分音符的时间，每分钟96拍
    private static final int baseStick = 96;

    public static void test() {
        byte[] b = MidiUtil.getDynamicBytes(65535);
        for (int i = 0; i <= b.length - 1; i++) {
            System.out.printf("%x ", b[i]);
        }
    }


    //把空格隔开的十六进制字符串写出流
    public static void writeHexByte(OutputStream outputStream, String hex) {
        try {
            for (String str : hex.split(" ")) {
                outputStream.write(Integer.parseInt(str, 16));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //向MIDI文件中记录按下某个音符;通道0-15,时间差tick,是baseStick的倍数,音符的范围是,0-127
    public static void press(OutputStream outputStream, float tick, int channel, int key) throws IOException {
        int t = (int) (tick * baseStick);
        //时间差
        outputStream.write(MidiUtil.getDynamicBytes(t));

    }

    //向MIDI设备通道中记录释放某个音符,通道0-15,时间差tick,音符的范围是,0-127
    public static void relase(OutputStream outputStream, float tick, int channel, int key) {

    }

    public static void test1() throws IOException {
        File file = new File("D:\\2.mid");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        //MThd，头部长度6
        writeHexByte(fileOutputStream, "4D 54 68 64");
        writeHexByte(fileOutputStream, "00 00 00 06");

        //00 单音轨；01多音轨；02多音轨不同步
        writeHexByte(fileOutputStream, "00 00");

        //轨道数：1个轨道
        writeHexByte(fileOutputStream, "00 01");
        //间隔时间：９６分之一秒
        writeHexByte(fileOutputStream, "00 60");
        //音轨开始，MTrk
        writeHexByte(fileOutputStream, "4D 54 72 6B");
        //音轨长度
        writeHexByte(fileOutputStream, "00 00 00 10");
        //时间 事件
        writeHexByte(fileOutputStream, "00 92 60 96");//时间、第三通道按下键，音符60，力度96
        writeHexByte(fileOutputStream, "60 91 43 40");
        writeHexByte(fileOutputStream, "78 91 43 40");

        writeHexByte(fileOutputStream, "00 FF 2F 00");
        fileOutputStream.flush();
        fileOutputStream.close();

    }

    public static void main(String[] args) throws IOException {
        test1();
        File file = new File("D:\\1.mid");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        //6字节文件头描述
        fileOutputStream.write('M');
        fileOutputStream.write('T');
        fileOutputStream.write('h');
        fileOutputStream.write('d');
        fileOutputStream.write(0);
        fileOutputStream.write(0);
        fileOutputStream.write(0);
        fileOutputStream.write(6);

        //00 单音轨；01多音轨；02多音轨不同步
        fileOutputStream.write(0);
        fileOutputStream.write(0);
        //轨道数，音轨数加1
        fileOutputStream.write(0);
        fileOutputStream.write(1);

        //基本时间,120四分音符每分钟
        fileOutputStream.write(0);
        fileOutputStream.write(60);


        //全局轨道
        fileOutputStream.write('M');
        fileOutputStream.write('T');
        fileOutputStream.write('r');
        fileOutputStream.write('k');
        //轨道字节数
        fileOutputStream.write(0);
        fileOutputStream.write(0);
        fileOutputStream.write(0);
        fileOutputStream.write(3);

        //按下一个音符C60
        writeHexByte(fileOutputStream, "00 C0 05");


        fileOutputStream.flush();
        fileOutputStream.close();
    }
}
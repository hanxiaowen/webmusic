package midi.test;

import midi.basic.Mode;
import midi.basic.Speed;
import midi.midi.MidiPlayer;
import midi.notation.Book;
import midi.notation.Note;
import midi.notation.Part;
import midi.notation.Section;
import midi.notation.util.BookUtil;
import midi.notation.util.NoteUtil;

import java.io.IOException;

/**
 * Created by Administrator on 2017/2/9.
 */
public class BookUtilTest {
    public static void main(String[] args) throws IOException {
        Book book = BookUtil.fromPianoFile("C:\\Users\\Administrator\\IdeaProjects\\MIDI\\Code\\res\\piano\\a.piano");
        //保存到mid文件
        BookUtil.toMidiFile(book, "d:\\1.mid");

        //播放
        MidiPlayer.play("d:\\1.mid");
    }
    public static void test1(String[] args) {
        //乐谱
        Book book = new Book();
        book.addMode(Mode.C, Mode.D);
        book.getSpeedList().add(Speed.speed112);
        book.setName("测试乐谱");

        //main part
        Part part = new Part();
        part.setPartName("main");
        Section section = new Section();
        Note note1 = NoteUtil.fromNoteString("w[C,D,E](velocity=100)");
        Note note2 = NoteUtil.fromNoteString("w[C,D,E](velocity=100)");
        Note note3 = NoteUtil.fromNoteString("w[C,D,E](velocity=100)");
        Note note4 = NoteUtil.fromNoteString("w[G](velocity=100)");
        Note note5 = NoteUtil.fromNoteString("w[A](velocity=100)");
        Note note6 = NoteUtil.fromNoteString("w[A](velocity=100)");
        Note note7 = NoteUtil.fromNoteString("w[G](velocity=100)");
        section.addNote(note1, note2, note3, note4,note5,note6,note7);
        part.addSection(section);
        book.addPart(part);

        //chord part
        Part part2 = new Part();
        part2.setPartName("chord");
        Section section2 = new Section();
        Note note11 = NoteUtil.fromNoteString("h[C-](velocity=60)");
        Note note22 = NoteUtil.fromNoteString("h[G-](velocity=60)");
        Note note33 = NoteUtil.fromNoteString("h[E-](velocity=60)");
        Note note44 = NoteUtil.fromNoteString("h[G-](velocity=60)");

        Note note55 = NoteUtil.fromNoteString("h[C-](velocity=60)");
        Note note66 = NoteUtil.fromNoteString("h[G-](velocity=60)");
        Note note77 = NoteUtil.fromNoteString("h[E-](velocity=60)");
        Note note78 = NoteUtil.fromNoteString("h[G-](velocity=60)");

        Note note25 = NoteUtil.fromNoteString("h[C-](velocity=60)");
        Note note26 = NoteUtil.fromNoteString("h[A-](velocity=60)");
        Note note27 = NoteUtil.fromNoteString("h[F-](velocity=60)");
        Note note28 = NoteUtil.fromNoteString("h[A-](velocity=60)");
        section2.addNote(note11, note22, note33, note44,note55,note66,note77,note78,note25,note26,note27,note28);
        part2.addSection(section2);
        book.addPart(part2);

        //保存到mid文件
        BookUtil.toMidiFile(book,"d:\\1.mid");

        //播放
        MidiPlayer.play("d:\\1.mid");

    }
}

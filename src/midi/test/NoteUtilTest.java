package midi.test;

import midi.basic.Pitch;
import midi.notation.Note;
import midi.notation.util.NoteUtil;
import midi.theory.PitchUtil;


public class NoteUtilTest {
    public static void text2() {
        Note note = NoteUtil.fromNoteString( "q+[bC,D+,#E](velocity=100,off=true,text=我)");
        for (Pitch pitch : note.pitchList) {
            System.out.println(PitchUtil.toString(pitch));
        }
        Note note2 = NoteUtil.fromNoteString("q+[C--,D,bbE++](velocity=100,off=true,text=我)");
        System.out.println(NoteUtil.toNoteString(note2));
    }

    public static void main(String[] args) {
        test1();


    }
    public static void test1(){
        String str="q[C]()";
        Note note=NoteUtil.fromNoteString(str);
        System.out.println(NoteUtil.toNoteString(note));
    }
}

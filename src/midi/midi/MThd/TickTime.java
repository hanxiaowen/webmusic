package midi.midi.MThd;

/**
 * 基本间隔时间
 */
public enum TickTime {
    Tick32(32),
    Tick48(48),
    Tick64(64),
    Tick80(80),
    Tick96(96),
    Tick112(112);
    private int value;

    TickTime(int value){
        this.value=value;
    }
    public int tickTimeValue() {
        return value;
    }
}

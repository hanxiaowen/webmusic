package midi.midi.MThd;

/**
 * Midi头部类型
 */
public enum MidiType {

    //单音轨
    Type00,
    //多音轨同步
    Type01,
    //多音轨不同步
    Type02;
}

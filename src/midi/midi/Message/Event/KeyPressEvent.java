package midi.midi.Message.Event;

/**
 * 按键下压Event
 */
public class KeyPressEvent extends MidiEvent {
    public int channel;
    public int key;
    public int velocity;
    public KeyPressEvent(int channel, int key, int velocity) {
        this.channel=channel;
        this.key=key;
        this.velocity=velocity;
    }
}

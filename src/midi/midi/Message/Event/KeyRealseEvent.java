package midi.midi.Message.Event;

/**
 * 按键释放Event
 */
public class KeyRealseEvent extends MidiEvent {
    public int channel;
    public int key;
    public int velocity;
    public KeyRealseEvent(int channel, int key, int velocity) {
        this.channel=channel;
        this.key=key;
        this.velocity=velocity;
    }
}

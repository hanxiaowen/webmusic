package midi.midi.MTrk;

import midi.midi.Message.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * mid文件轨道部分
 */
public class MTrk {

    //事件
    private List<Message> messageList = new ArrayList<Message>();

    public void addMessage(Message message) {
        this.messageList.add(message);
    }

    public List<Message> getMessageList() {
        return messageList;
    }
}

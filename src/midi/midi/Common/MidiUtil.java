package midi.midi.Common;

import java.io.*;
import java.util.Stack;

/**
 * Midi工具类
 * byte范围：-128~127
 * midi格式：4d 54 68 64 00 00 00 06 ff ff nn nn dd dd
 * ff ff轨道:00 00 单轨道
 *          00 01 同步多轨道
 *          00 02 不同步多轨道
 * nn nn 轨道个数，为实际轨道个数+1个全局音轨
 * dd dd 基本时间类型
 */
public class MidiUtil {
    public static byte[] intToByteArray(int i) {
        byte[] result = new byte[4];
        //由高位到低位
        result[0] = (byte) ((i >> 24) & 0xFF);
        result[1] = (byte) ((i >> 16) & 0xFF);
        result[2] = (byte) ((i >> 8) & 0xFF);
        result[3] = (byte) (i & 0xFF);
        return result;
    }

    //字节转二进制
    public static void printByteBin(byte b) {
        if (b == -128) {
            System.out.print("10000000");
            return;
        }
        //十进制数
        int a = b;
        if (b < 0) a = -b;
        //声明一个栈
        Stack stack = new Stack();

        //转换成二进制过程
        while (a != 0) {
            //余数入栈
            stack.push(a % 2);
            a = a / 2;
        }

        //补0
        int k = 7 - (stack.size() % 8);
        for (int i = 1; i <= k; i = i + 1) {
            stack.push((byte) (0));
        }

        //符号位
        if (b < 0) stack.push(1);
        if (b >= 0) stack.push(0);


        //余数出栈
        while (!stack.isEmpty()) {

            System.out.print(stack.pop());

        }


    }

    //整数转二进制打印,范围0-255
    public static void printBin(int v) {
        //十进制数
        int a = v;

        //声明一个栈
        Stack stack = new Stack();

        //转换成二进制过程
        while (a != 0) {
            //余数入栈
            stack.push(a % 2);
            a = a / 2;
        }


        int k = 8 - stack.size();
        for (int i = 1; i <= k; i = i + 1) {
            stack.push((byte) (0));
        }


        while (!stack.isEmpty()) {
            System.out.print(stack.pop());

        }
        System.out.print(" ");
    }

    //得到一个整数的变长字节
    public static byte[] getDynamicBytes(int value) {

        if (value == 0) {
            byte[] br = {0};
            return br;
        }

        //声明一个栈
        Stack<Byte> stack = new Stack();
        int a = value;
        //转换成128进制过程
        while (a > 0) {
            //余数入栈
            stack.push((byte) (a % 128));
            a = a / 128;
        }
        byte[] b = new byte[stack.size()];

        //余数出栈,非最低位+128
        int i = 0;
        while (!stack.isEmpty()) {
            if (i % 8 == 0) System.out.print(" ");
            if (stack.size() > 1) {
                b[i] = (byte) (stack.pop() + 128);
            } else if (stack.size() == 1) {
                //最低位不加
                b[i] = stack.pop();
            }
            i++;
        }
        return b;
    }

    public static void main(String[] args) throws IOException {
        testMid();
    }

    static void testMid() throws IOException {
        InputStream inputStream = new FileInputStream(new File("c:\\基本音阶.mid"));
        int b = 0;
        int k = 0;
        while ((b = inputStream.read()) != -1) {
            printBin(b);
            System.out.print(Integer.toHexString(b)+"\t");
            k++;
            if (k % 10 == 0) System.out.println("");
        }
    }
}

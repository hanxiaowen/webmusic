package midi.midi.Common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**

 */
public class ByteReader {


    public static void main(String[] args) throws IOException {
        //File file = new File("d:\\3.mid");
        File file = new File("c:\\基本音阶.mid");
        FileInputStream fileOutputStream = new FileInputStream(file);
        byte b[] = new byte[8];
        while (fileOutputStream.read(b) != -1) {
            System.out.print("HEX:\t");
            for (int i = 0; i <= b.length - 1; i++) {
                System.out.printf("%x", b[i]);
                System.out.print("\t\t\t");
            }

            System.out.print("\n");
            System.out.print("DEC:\t");
            for (int i = 0; i <= b.length - 1; i++) {
                System.out.print(b[i]);
                System.out.print("\t\t\t");
            }
            System.out.print("\n");
            System.out.print("BIN:\t");
            for (int i = 0; i <= b.length - 1; i++) {
                MidiUtil.printByteBin(b[i]);
                System.out.print("\t");
            }


            System.out.print("\n");
            System.out.print("ASC:\t");
            for (int i = 0; i <= b.length - 1; i++) {
                System.out.printf(String.valueOf((char) (b[i])));
                System.out.print("\t\t\t");
            }

            System.out.println("\n---------------------------------------------------------------------------------------------------------");
            b = new byte[8];
        }
    }
}

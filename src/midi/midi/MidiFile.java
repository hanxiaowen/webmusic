package midi.midi;

import midi.midi.MThd.MThd;
import midi.midi.MTrk.MTrk;

import java.util.ArrayList;
import java.util.List;

/*
 * 表示一个midi文件
 * Midi工具类
 * byte范围：-128~127
 * midi格式：4d 54 68 64 00 00 00 06 ff ff nn nn dd dd
 * ff ff轨道:00 00 单轨道
 *          00 01 同步多轨道
 *          00 02 不同步多轨道
 * nn nn 轨道个数，为实际轨道个数+1个全局音轨
 * dd dd 基本时间类型
 */
public class MidiFile {
    //头部
    private MThd mThd;
    //轨道
    private List<MTrk> mTrkList=new ArrayList<MTrk>();

    public MidiFile() {
    }

    public void addMTrk(MTrk mTrk){
        mTrkList.add(mTrk);
    }
    public MidiFile(MThd mThd, List<MTrk> mTrkList) {
        this.mThd = mThd;
        this.mTrkList = mTrkList;
    }

    public MThd getmThd() {
        return mThd;
    }

    public void setmThd(MThd mThd) {
        this.mThd = mThd;
    }

    public List<MTrk> getmTrkList() {
        return mTrkList;
    }

    public void setmTrkList(List<MTrk> mTrkList) {
        this.mTrkList = mTrkList;
    }
}

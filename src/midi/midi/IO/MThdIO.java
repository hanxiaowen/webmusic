package midi.midi.IO;

import midi.midi.MThd.MThd;
import midi.midi.MThd.MidiType;
import midi.midi.MThd.TickTime;

/**
 * 负责把一个Mthd转换为byte
 */
public class MThdIO {
    public static byte[] getByte(MThd mthd) {
        //M T h d 0 0 0 6
        byte[] bytes = new byte[14];
        bytes[0] = 'M';
        bytes[1] = 'T';
        bytes[2] = 'h';
        bytes[3] = 'd';
        bytes[4] = 0;
        bytes[5] = 0;
        bytes[6] = 0;
        bytes[7] = 6;

        //n文件类型
        bytes[9] = 0;
        if (mthd.midiType == MidiType.Type00) {
            bytes[9] = 0;
        } else if (mthd.midiType == MidiType.Type01) {
            bytes[9] = 1;
        } else if (mthd.midiType == MidiType.Type02) {
            bytes[9] = 2;
        }
        //轨道数目
        int mTrkNum = mthd.MtrkNum;
        if (mTrkNum > 16) {
            System.err.println("mTrkNum>16");
            return null;
        }
        bytes[10] = 0;
        bytes[11] = (byte) mTrkNum;

        //基本时间
        if (mthd.tickTime == TickTime.Tick32) {
            bytes[12] = 0;
            bytes[13] = 32;
        } else if (mthd.tickTime == TickTime.Tick48) {
            bytes[12] = 0;
            bytes[13] = 48;
        }  else if (mthd.tickTime == TickTime.Tick64) {
            bytes[12] = 0;
            bytes[13] = 64;
        } else if (mthd.tickTime == TickTime.Tick80) {
            bytes[12] = 0;
            bytes[13] = 80;
        }else if (mthd.tickTime == TickTime.Tick96) {
            bytes[12] = 0;
            bytes[13] = 96;
        }else if (mthd.tickTime == TickTime.Tick112) {
            bytes[12] = 0;
            bytes[13] = 112;
        }
        //返回
        return bytes;
    }
}

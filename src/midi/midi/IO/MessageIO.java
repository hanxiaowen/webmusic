package midi.midi.IO;

import midi.midi.Message.Event.FinalEvent;
import midi.midi.Message.Event.KeyPressEvent;
import midi.midi.Message.Event.KeyRealseEvent;
import midi.midi.Message.Message;

public class MessageIO {

    public static byte[] getByte(Message message) {

        if (message.event.getClass() == FinalEvent.class) {
            //结束消息
            byte[] bytes = new byte[4];
            bytes[0] = 0x00;
            bytes[1] = (byte) 0xFF;
            bytes[2] = (byte) 0x2F;
            bytes[3] = (byte) 0x00;
            return bytes;
        } else if (message.event.getClass() == KeyPressEvent.class) {

            //键按下消息
            KeyPressEvent keyPressEvent = (KeyPressEvent) message.event;
            byte[] b = midi.midi.Common.MidiUtil.getDynamicBytes(message.tick);
            int len = b.length;
            byte[] r = new byte[len + 3];
            //时间差
            for (int i = 0; i <= len - 1; i++) {
                r[i] = b[i];
            }
            byte p = (byte) ((9 << 4) + (keyPressEvent.channel));
            //通道按下某键
            r[len + 0] = p;
            //音
            r[len + 1] = (byte) keyPressEvent.key;
            //速度
            r[len + 2] = (byte) keyPressEvent.velocity;

            return r;
        } else if (message.event.getClass() == KeyRealseEvent.class) {

            //键按释放消息
            KeyRealseEvent KeyRealseEvent = (KeyRealseEvent) message.event;
            byte[] b = midi.midi.Common.MidiUtil.getDynamicBytes(message.tick);
            int len = b.length;
            byte[] r = new byte[len + 3];
            //时间差
            for (int i = 0; i <= len - 1; i++) {
                r[i] = b[i];
            }
            byte p = (byte) ((8 << 4) + (KeyRealseEvent.channel));
            //通道按下某键
            r[len + 0] = p;
            //音
            r[len + 1] = (byte) KeyRealseEvent.key;
            //速度
            r[len + 2] = (byte) KeyRealseEvent.velocity;

            return r;
        }
        return null;
    }
}

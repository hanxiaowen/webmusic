package midi.midi.IO;

import midi.midi.MThd.MThd;
import midi.midi.MTrk.MTrk;
import midi.midi.MidiFile;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Midi文件写出
 */
public class MidiFileIO {


    public static void write(OutputStream outputStream, MidiFile midiFile) throws IOException {
        MThd mThd = midiFile.getmThd();
        outputStream.write(MThdIO.getByte(mThd));
        for (MTrk mtrk : midiFile.getmTrkList()) {
            outputStream.write(MTrkIO.getByte(mtrk));
        }
    }
}

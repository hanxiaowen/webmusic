package midi.basic;

/**
 * 拍子长度
 */
public enum Duration {
    whole("全音符", "w", 1.0),

    half("二分音符", "h", 0.5),
    halfDotted("二分附点音符", "h+", 0.75),

    quarter("四分音符", "q", 0.25),
    quaraterDotted("四分附点音符", "q+", 0.375),

    eighth("八分音符", "e", 0.125),
    eighthDotted("八分附点音符", "e+", 0.1875),

    sixteenth("十六分音符", "s", 0.0625),
    sixteenthDotted("十六分附点音符", "s+", 0.09375);

    String name;
    String desc;
    double value;

    public String getDesc() {
        return desc;
    }

    public double getValue() {
        return value;
    }

    Duration(String name, String desc, double value) {
        this.name = name;
        this.desc=desc;
        this.value=value;
    }

    //从w、h等拍子长度字符串构建
    public static Duration fromDurationString(String str) {
        switch (str) {
            case "w":
                return Duration.whole;
            case "h":
                return Duration.half;
            case "q":
                return Duration.quarter;
            case "e":
                return Duration.eighth;
            case "s":
                return Duration.sixteenth;
            case "h+":
                return Duration.halfDotted;
            case "q+":
                return Duration.quaraterDotted;
            case "e+":
                return Duration.eighthDotted;
            case "s+":
                return Duration.sixteenthDotted;
        }
        return null;

    }
}

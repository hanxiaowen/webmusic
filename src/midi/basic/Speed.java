package midi.basic;

/**
 * 音乐速度,越大越快
 */
public enum Speed {
    speed32(32),
    speed48(48),
    speed64(64),
    speed80(80),
    speed96(96),
    speed112(112);

    int speed;

    public int getSpeed() {
        return speed;
    }


    Speed(int speed) {
        this.speed = speed;
    }

    public static Speed fromSpeed(int speed) {
        if (speed == 112) {
            return Speed.speed112;
        } else if (speed == 96) {
            return Speed.speed96;
        } else if (speed == 80) {
            return Speed.speed80;
        } else if (speed == 64) {
            return Speed.speed64;
        } else if (speed == 48) {
            return Speed.speed48;
        } else if (speed == 32) {
            return Speed.speed32;
        } else {
            return null;
        }
    }


}

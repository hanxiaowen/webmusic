package midi.basic;

/**
 * 音的音名表示法
 * 音名=自然音阶枚举+八度位置+升降符号 三参数确定，它的id也确定了
 */
public class Pitch extends Voice {
    //音名枚举
    private Scale scale;
    //八度位置
    private int position;
    //升降符号个数
    private int offset;


    public Scale getScale() {
        return scale;
    }

    public int getOffset() {
        return offset;
    }

    public int getPosition() {
        return position;
    }

    public void setScale(Scale scale) {
        this.scale = scale;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }


    public Pitch() {
        this.position = 0;
        this.offset = 0;
    }

    //从音名枚举和位置实例化音名
    public Pitch(Scale scale, int position) {
        this.scale = scale;
        this.position = position;
        this.id = Configure.MainC + scale.offset() + 12 * position;
    }


    //获取音高编号0-127
    public int id() {
        return id;
    }

    //转换成字符串表示形式
    public String toString() {
        return String.format("[%s,%d]", this.scale.scaleString(), position);
    }
}

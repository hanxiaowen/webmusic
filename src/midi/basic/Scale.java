package midi.basic;

/**
 * 音阶枚举
 * 每一个枚举关联一个相对C的偏移量和音名的字符表示
 */
public enum Scale {

    C(0, "C"),
    D(2, "D"),
    E(4, "E"),
    F(5, "F"),
    G(7, "G"),
    A(9, "A"),
    B(11, "B");

    private int offset;
    private String scaleString;

    //偏移量和音符名
    Scale(int offset, String scaleString) {
        this.offset = offset;
        this.scaleString = scaleString;
    }

    public int offset() {
        return offset;
    }

    public String scaleString() {
        return scaleString;
    }

    //通过偏移量来构建一个音阶枚举
    public static Scale fromOffSet(int offset) {
        if (offset == 0) {
            return Scale.C;
        } else if (offset == 2) {
            return Scale.D;
        } else if (offset == 4) {
            return Scale.E;
        } else if (offset == 5) {
            return Scale.F;
        } else if (offset == 7) {
            return Scale.G;
        } else if (offset == 9) {
            return Scale.A;
        } else if (offset == 11) {
            return Scale.B;
        } else {
            return null;
        }
    }

    //通过字符串来构建一个音符枚举
    public static Scale fromScaleString(String str) {
        if (str.equals("C")) {
            return Scale.C;
        } else if (str.equals("D")) {
            return Scale.D;
        } else if (str.equals("E")) {
            return Scale.E;
        } else if (str.equals("F")) {
            return Scale.F;
        } else if (str.equals("G")) {
            return Scale.G;
        } else if (str.equals("A")) {
            return Scale.A;
        } else if (str.equals("B")) {
            return Scale.B;
        } else {
            return null;
        }
    }

}

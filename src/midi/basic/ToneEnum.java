package midi.basic;

/**
 * 枚举所有唱名名称
 * 每一个唱名有一个相对d的偏移量
 */
public enum ToneEnum {

    d(0, "Do"),   //do  0
    ds(1, "#Do"),

    rb(1, "bRe"),
    r(2, "Re"),   //re  2
    rs(3, "#Re"),

    mb(3, "bMi"),
    m(4, "Mi"),   //mi  4

    f(5, "Fa"),   //fa  5
    fs(6, "#Fa"),

    sb(6, "bSo"),
    s(7, "So"),  //so
    ss(8, "#So"),

    lb(8, "bLa"),
    l(9, "La"),  //la
    ls(10, "#La"),

    xb(10, "bXi"),
    x(11, "Xi")  //xi
    ;

    private int offset;
    private String toneString;

    ToneEnum(int offset, String toneString) {
        this.offset = offset;
        this.toneString = toneString;
    }

    public String getToneString() {
        return toneString;
    }

    public int offset() {
        return offset;
    }

    //从音编号和调式构建
    public static ToneEnum fromId(int id, Mode mode) {//60 c
        int a = (id - 60 - mode.getOffset()) % 12;
        if (a < 0) {
            a = 12 - (-a);
        }
        if (a == 0) {
            return ToneEnum.d;
        } else if (a == 1) {
            return ToneEnum.ds;
        } else if (a == 2) {
            return ToneEnum.r;
        } else if (a == 3) {
            return ToneEnum.rs;
        } else if (a == 4) {
            return ToneEnum.m;
        } else if (a == 5) {
            return ToneEnum.f;
        } else if (a == 6) {
            return ToneEnum.fs;
        } else if (a == 7) {
            return ToneEnum.s;
        } else if (a == 8) {
            return ToneEnum.ss;
        } else if (a == 9) {
            return ToneEnum.l;
        } else if (a == 10) {
            return ToneEnum.ls;
        } else if (a == 11) {
            return ToneEnum.x;
        } else {
            return null;
        }
    }
}

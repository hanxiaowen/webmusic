package midi.basic;

/**
 *音的抽象类
 * 每一个音拥有一个编号id0-127
 */
public abstract class Voice {
    //音编号0-127
    public int id;
    //获取音编号0-127
    public abstract int id();
}

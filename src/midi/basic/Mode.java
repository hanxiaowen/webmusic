package midi.basic;

//调式枚举
public enum Mode {
    Ab(-4, "Ab"),
    A(-3, "A"),//

    Bb(-2, "Bb"),
    B(-1, "B"),//

    Cb(-1, "Cb"),
    C(0, "C"),//0
    Cs(1, "Cs"),

    Db(1, "Db"),
    D(2, "D"),//+2

    Eb(3, "Eb"),
    E(4, "E"),//+4

    F(5, "F"),//+5
    Fs(6, "Fs"),

    Gb(6, "Gb"),
    G(7, "G")//+7
    ;
    int offset;
    String modeString;

    Mode(int offset, String modeString) {
        this.offset = offset;
        this.modeString = modeString;
    }

    public static Mode fromString(String str) {
        if (str.equals("Ab")) {
            return Mode.Ab;
        }
        if (str.equals("A")) {
            return Mode.A;
        }
        if (str.equals("Bb")) {
            return Mode.B;
        }
        if (str.equals("B")) {
            return Mode.B;
        }
        if (str.equals("Cb")) {
            return Mode.Cb;
        }
        if (str.equals("C")) {
            return Mode.C;
        }
        if (str.equals("Cs")) {
            return Mode.Cs;
        }
        if (str.equals("Db")) {
            return Mode.Db;
        }
        if (str.equals("D")) {
            return Mode.D;
        }
        if (str.equals("Eb")) {
            return Mode.Eb;
        }
        if (str.equals("E")) {
            return Mode.E;
        }
        if (str.equals("F")) {
            return Mode.F;
        }
        if (str.equals("Fs")) {
            return Mode.Fs;
        }
        if (str.equals("Gb")) {
            return Mode.Gb;
        }
        if (str.equals("G")) {
            return Mode.G;
        }
        return null;


    }

    public int getOffset() {
        return this.offset;
    }

    public String getModeString() {
        return modeString;
    }
}

package midi.basic;

/**
 * 节奏
 */
public enum Beat {

    //二二拍子
    Beat22,
    Beat24,
    Beat34,
    Beat44,
    Beat48,
    Beat38,
    Beat68,;

    //
    public static Beat formString(String string) {
        if (string.equals("2/2")) {
            return Beat22;
        } else if (string.equals("2/4")) {
            return Beat24;
        }
        if (string.equals("3/4")) {
            return Beat34;
        }
        if (string.equals("4/4")) {
            return Beat44;
        }
        if (string.equals("4/8")) {
            return Beat48;
        }
        if (string.equals("3/8")) {
            return Beat38;
        }
        if (string.equals("6/8")) {
            return Beat68;
        }
        return null;
    }
}

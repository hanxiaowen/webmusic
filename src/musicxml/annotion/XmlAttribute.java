package musicxml.annotion;

import java.lang.annotation.*;

//XML属性
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Inherited
public @interface XmlAttribute {
    String name() default "default";

}

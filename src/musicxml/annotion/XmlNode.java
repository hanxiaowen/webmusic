package musicxml.annotion;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Inherited
public @interface XmlNode {
    String name() default "default";

    String desc() default "结点";
}

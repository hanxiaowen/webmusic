package musicxml.annotion;

import java.lang.annotation.*;

//XML文本内容
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Inherited
public @interface XmlText {

}

package musicxml;

import musicxml.annotion.XmlAttribute;
import musicxml.annotion.XmlNode;
import musicxml.part.Part;
import musicxml.partlist.PartList;
import musicxml.partlist.PartName;
import musicxml.partlist.ScorePart;
import musicxml.work.Work;
import musicxml.work.WorkTitle;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

//乐谱描述文件格式
@XmlNode(name = "score-partwise", desc = "乐谱")
public class ScorePartwise extends Node {

    //版本
    @XmlAttribute
    public String version = "3";


    public ScorePartwise() {
    }

    //创建乐谱
    public ScorePartwise(String workTitle) {
        Work work = new Work(new WorkTitle(workTitle));
        this.addWork(work);
    }

    //增加分谱信息
    public ScorePart addScorePart(String id, String name) {
        ScorePart scorePart = new ScorePart(id, new PartName(name));
        PartList partList = getPartList();
        if (partList == null) {
            partList = new PartList();
            this.addPartList(partList);
        }
        partList.addScorePart(scorePart);
        addPart(new Part(id));
        return scorePart;
    }


    //工作信息
    public ScorePartwise addWork(Work work) {
        this.nodeList.add(work);
        return this;
    }


    //声部列表
    public ScorePartwise addPartList(PartList partList) {
        this.nodeList.add(partList);
        return this;
    }

    //获取声部列表
    public PartList getPartList() {
        for (Node node : nodeList) {
            if (node instanceof PartList) {
                return (PartList) node;
            }
        }
        return null;
    }

    //声部内容
    public ScorePartwise addPart(Part part) {
        this.nodeList.add(part);
        return this;
    }


    //获取声部
    public Part getPart(String partId) {
        for (Node node : nodeList) {
            if (node instanceof Part) {
                Part p = (Part) node;
                if (p.id.equals(partId)) {
                    return p;
                }
            }
        }
        return null;
    }


    //转换成midi
    public void toMidi(String path) {
    }


}

package musicxml.work;

import musicxml.Node;
import musicxml.annotion.XmlNode;

//工作信息
@XmlNode(name = "work", desc = "工作")
public class Work extends Node {

    public Work(){}

    public Work(WorkTitle workTitle) {
        this.addWorkTitle(workTitle);
    }

    //标题
    public void addWorkTitle(WorkTitle workTitle) {
        this.nodeList.add(workTitle);
    }
}

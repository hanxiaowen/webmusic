package musicxml.work;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

//标题
@XmlNode(name = "work-title", desc = "谱名")
public class WorkTitle extends Node {

    @XmlText
    public String value = "";

    //标题内容
    public WorkTitle(String value) {
        this.value = value;
    }

    public static void main(String[] args) {
        Node node = new WorkTitle("测试");
        System.out.println(node.toXML());
    }
}

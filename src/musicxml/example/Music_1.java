package musicxml.example;

import musicxml.ScorePartwise;
import musicxml.part.Part;
import musicxml.part.measure.Measure;
import musicxml.part.measure.attributes.Attributes;
import musicxml.part.measure.attributes.Division;
import musicxml.part.measure.attributes.clef.Clef;
import musicxml.part.measure.attributes.key.Key;
import musicxml.part.measure.attributes.time.Time;
import musicxml.part.measure.note.Duration;
import musicxml.part.measure.note.Note;
import musicxml.part.measure.note.Staff;
import musicxml.part.measure.note.Type;
import musicxml.part.measure.note.pitch.Alter;
import musicxml.part.measure.note.pitch.Octave;
import musicxml.part.measure.note.pitch.Pitch;
import musicxml.part.measure.note.pitch.Step;

public class Music_1 {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        //创建乐谱
        ScorePartwise scorePartwise = new ScorePartwise("music1");

        //设置分谱和乐器名字
        scorePartwise.addScorePart("p1", "piano");

        //分谱
        Part part = scorePartwise.getPart("p1");

        //小节
        Measure measure = new Measure(1);
        Attributes attributes = new Attributes();
        //division要最先设置
        attributes.addDivision(Division.D256()).addKey(Key.MajorInC()).addTime(Time.Beat44());
        attributes.addStaves(2).addClef(Clef.G(1)).addClef(Clef.F(2));
        measure.addAttributes(attributes);
        part.addMeasure(measure);

        //音符
        Note note = new Note();
        note.addPitch(new Step("C"), new Alter(0), new Octave(4));
        note.addDuration(1024);
        note.addType(Type.TYPE_whole);
        note.addStaff(1);
        measure.addNote(note);

        //转换到xml格式
        String s = scorePartwise.toXML();
        scorePartwise.toFile("music1.xml");
        System.out.println(s);

    }
}
package musicxml.example;

import musicxml.ScorePartwise;
import musicxml.part.Part;
import musicxml.part.measure.Measure;
import musicxml.part.measure.attributes.Attributes;
import musicxml.part.measure.attributes.Division;
import musicxml.part.measure.attributes.clef.Clef;
import musicxml.part.measure.attributes.clef.Line;
import musicxml.part.measure.attributes.clef.Sign;
import musicxml.part.measure.attributes.key.Fifths;
import musicxml.part.measure.attributes.key.Key;
import musicxml.part.measure.attributes.key.Mode;
import musicxml.part.measure.attributes.time.BeatType;
import musicxml.part.measure.attributes.time.Beats;
import musicxml.part.measure.attributes.time.Time;
import musicxml.part.measure.note.*;
import musicxml.part.measure.note.pitch.Alter;
import musicxml.part.measure.note.pitch.Octave;
import musicxml.part.measure.note.pitch.Pitch;
import musicxml.part.measure.note.pitch.Step;
import musicxml.partlist.PartList;
import musicxml.partlist.PartName;
import musicxml.partlist.ScorePart;
import musicxml.work.Work;
import musicxml.work.WorkTitle;

import java.io.File;

public class Music_2 {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        //创建乐谱
        ScorePartwise scorePartwise = new ScorePartwise();
        scorePartwise.addWork(new Work(new WorkTitle("测试xxx")));
        scorePartwise.addPartList(new PartList(new ScorePart("p1", new PartName("p1"))));
        Part part = new Part("p1");

        //小节
        Measure measure = new Measure();


        //属性
        Attributes attributes = new Attributes();
        Division division = new Division(256);
        Key key = new Key().addFifths(new Fifths(0)).addMode(new Mode("major"));
        Time time = new Time(new Beats(4), new BeatType(4));
        Clef clef = new Clef(1, new Sign("G"), new Line(2));
        attributes.addDivision(division).addKey(key).addTime(time).addClef(clef);
        measure.addAttributes(attributes);

        part.addMeasure(measure);
        scorePartwise.addPart(part);

        //音符1
        Note note1 = new Note();

        note1.addPitch(new Pitch().addStep(new Step("C")).addAlter(new Alter(0)).addOctave(new Octave(4)));
        note1.addDuration(new Duration(1024));
        note1.addType(new Type(Type.TYPE_whole));
        note1.addStaff(new Staff(1));

        //音符2
        Note note2 = new Note();
        note2.addChord(new Chord());
        note2.addPitch(new Pitch().addStep(new Step("D")).addAlter(new Alter(0)).addOctave(new Octave(4)));
        note2.addDuration(new Duration(1024));
        note2.addType(new Type(Type.TYPE_whole));
        note2.addStaff(new Staff(1));


        measure.addNote(note1);
        measure.addNote(note2);
        String s = scorePartwise.toXML();
        scorePartwise.toFile("测试1.musicxml");
        System.out.println(s);

    }
}
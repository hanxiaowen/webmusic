package musicxml.partlist;

import musicxml.Node;
import musicxml.annotion.XmlAttribute;
import musicxml.annotion.XmlNode;

//声部
@XmlNode(name = "score-part",desc = "声部")
public class ScorePart extends Node {


    @XmlAttribute
    public String id = "";

    public ScorePart() {
    }

    public ScorePart(String id,PartName partName) {
        this.id = id;
        this.addPartName(partName);
    }

    public ScorePart(PartName partName) {
        this.addPartName(partName);
    }


    //声部名字
    public void addPartName(PartName partName) {
        this.nodeList.add(partName);
    }
}

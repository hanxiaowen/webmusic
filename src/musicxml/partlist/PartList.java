package musicxml.partlist;

import musicxml.Node;
import musicxml.annotion.XmlNode;

//声部列表
@XmlNode(name = "part-list", desc = "谱表")
public class PartList extends Node {

    public  PartList(){}

    public PartList(ScorePart scorePart) {
        this.addScorePart(scorePart);
    }

    //声部
    public void addScorePart(ScorePart scorePart) {
        this.nodeList.add(scorePart);
    }
}

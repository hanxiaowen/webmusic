package musicxml.partlist;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;
//声部名字
@XmlNode(name = "part-name", desc = "名字")
public class PartName extends Node {

    @XmlText
    public String value;

    public PartName(String value) {
        this.value = value;
    }
}

package musicxml.part.measure.attributes.clef;

import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;
import musicxml.part.measure.note.Note;

@XmlNode(name = "sign", desc = "谱号")
public class Sign extends Note {

    @XmlText
    public String value = "";

    //高音，中音，低音 谱表
    public static String SIGN_G = "G";
    public static String SIGN_F = "F";
    public static String SIGN_C = "C";

    public Sign(String value) {
        this.value = value;
    }
}

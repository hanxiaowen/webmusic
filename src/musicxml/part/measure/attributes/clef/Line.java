package musicxml.part.measure.attributes.clef;

import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;
import musicxml.part.measure.note.Note;

@XmlNode(name = "line")
public class Line extends Note {

    @XmlText
    public int value = 0;

    //GFC谱表的线高
    public static int LINE_G = 2;
    public static int LINE_F = 4;
    public static int LINE_C = 4;

    public Line(int value) {
        this.value = value;
    }
}

package musicxml.part.measure.attributes.clef;

import musicxml.Node;
import musicxml.annotion.XmlAttribute;
import musicxml.annotion.XmlNode;

//谱号
@XmlNode(name = "clef")
public class Clef extends Node {

    @XmlAttribute
    public int number = 0;

    //G谱表
    public static Clef G(int number) {
        return new Clef(number, new Sign(Sign.SIGN_G), new Line(Line.LINE_G));
    }

    //F谱表
    public static Clef F(int number) {
        return new Clef(number, new Sign(Sign.SIGN_F), new Line(Line.LINE_F));
    }


    //C谱表
    public static Clef C(int number) {
        return new Clef(number, new Sign(Sign.SIGN_C), new Line(Line.LINE_C));
    }


    public Clef(int number, Sign sign, Line line) {
        this.number = number;
        this.addSign(sign);
        this.addLine(line);
    }

    //谱号
    public void addSign(Sign sign) {
        this.nodeList.add(sign);
    }

    //线高
    public void addLine(Line line) {
        this.nodeList.add(line);
    }
}

package musicxml.part.measure.attributes.key;

import musicxml.Node;
import musicxml.annotion.XmlNode;

//调号和升降号
@XmlNode(name = "key",desc = "调式")
public class Key extends Node {


    public static Key MajorInC(){
        return new Key(Fifths.C(),new Mode(Mode.KEY_MAJOR));
    }


    public Key() {
    }


    public Key(Fifths fifths, Mode mode) {
        this.addFifths(fifths);
        this.addMode(mode);
    }

    //升降调个数
    public Key addFifths(Fifths fifths) {
        this.nodeList.add(fifths);
        return this;
    }

    //调性
    public Key addMode(Mode mode) {
        this.nodeList.add(mode);
        return this;
    }
}

package musicxml.part.measure.attributes.key;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

//升降调个数
@XmlNode(name = "fifths",desc = "五度")
public class Fifths extends Node {

    @XmlText
    public int value = 0;
    public static int SHARP_C = -7;
    public static int SHARP_G = -6;
    public static int SHARP_D = -5;
    public static int SHARP_A = -4;
    public static int SHARP_E = -3;
    public static int SHARP_B = -2;
    public static int F = -1;
    public static int C = 0;
    public static int G = 1;
    public static int D = 2;
    public static int A = 3;
    public static int E = 4;
    public static int B = 5;
    public static int FLAT_F = 6;
    public static int FLAT_C = 7;

    public static Fifths C() {
        return new Fifths(C);
    }

    public Fifths() {
    }

    public Fifths(int value) {
        this.value = value;
    }
}

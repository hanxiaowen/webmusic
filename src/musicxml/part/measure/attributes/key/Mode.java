package musicxml.part.measure.attributes.key;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

@XmlNode(name = "mode", desc = "类型")
public class Mode extends Node {

    //大调式
    public static String KEY_MAJOR = "major";

    //小调式
    public static String KEY_MINOR = "minor";


    @XmlText
    public String value = "";

    public Mode() {
    }

    public Mode(String value) {
        this.value = value;
    }
}

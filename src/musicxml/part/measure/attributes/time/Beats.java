package musicxml.part.measure.attributes.time;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

@XmlNode(name = "beats", desc = "节奏")
public class Beats extends Node {

    @XmlText
    public int value = 0;

    public Beats(int value) {
        this.value = value;
    }
}

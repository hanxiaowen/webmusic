package musicxml.part.measure.attributes.time;


import midi.basic.Beat;
import musicxml.annotion.XmlNode;
import musicxml.part.measure.note.Note;

//节奏
@XmlNode(name = "time", desc = "时间")
public class Time extends Note {

    //44拍
    public static Time Beat44() {
        Beats beats = new Beats(4);
        BeatType beatType = new BeatType(4);
        return new Time(beats, beatType);
    }


    public Time(Beats beats, BeatType beatType) {
        this.nodeList.add(beats);
        this.nodeList.add(beatType);
    }

    //节奏
    public void addBeats(Beats beats) {
        this.nodeList.add(beats);
    }

    //节奏类型
    public void addBeatType(BeatType beatType) {
        this.nodeList.add(beatType);
    }
}

package musicxml.part.measure.attributes.time;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

@XmlNode(name = "beat-type", desc = "类型")
public class BeatType extends Node {

    @XmlText
    public int value = 0;

    public BeatType(int value) {
        this.value = value;
    }
}

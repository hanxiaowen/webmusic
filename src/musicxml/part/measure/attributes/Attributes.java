package musicxml.part.measure.attributes;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.part.measure.attributes.clef.Clef;
import musicxml.part.measure.attributes.key.Key;
import musicxml.part.measure.attributes.time.Time;

//小节属性
@XmlNode(name = "attributes",desc = "属性")
public class Attributes extends Node {

    //四分音符时长
    public Attributes addDivision(Division division) {
         this.nodeList.add(division);
         return this;
    }

    //调号
    public Attributes addKey(Key key) {
        this.nodeList.add(key);
        return this;
    }

    //节奏
    public Attributes addTime(Time time) {
        this.nodeList.add(time);
        return this;
    }


    //谱个数
    public Attributes addStaves(int number){
        Staves staves=new Staves(number);
        this.nodeList.add(staves);
        return this;
    }

    //谱号
    public Attributes addClef(Clef clef) {
        this.nodeList.add(clef);
        return this;
    }
}

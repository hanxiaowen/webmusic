package musicxml.part.measure.attributes;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

//四分音符时长，这里是divisions
@XmlNode(name = "divisions",desc = "时长")
public class Division extends Node {


    //四分音符时值256
    public static Division D256() {
        return new Division(256);
    }

    //四分音符时间值默认256
    @XmlText
    public int value = 256;

    public Division() {
    }

    public Division(int value) {
        this.value = value;
    }
}

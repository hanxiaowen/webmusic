package musicxml.part.measure.attributes;

import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;
import musicxml.part.measure.note.Note;

//声部的个数
@XmlNode(name = "staves", desc = "个数")
public class Staves extends Note {

    @XmlText
    public int value;

    public Staves(int value) {
        this.value = value;
    }
}

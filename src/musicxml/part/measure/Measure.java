package musicxml.part.measure;

import musicxml.Node;
import musicxml.annotion.XmlAttribute;
import musicxml.annotion.XmlNode;
import musicxml.part.measure.attributes.Attributes;
import musicxml.part.measure.backup.Backup;
import musicxml.part.measure.note.Note;

//小节
@XmlNode(name = "measure", desc = "小节")
public class Measure extends Node {

    //序号
    @XmlAttribute
    public int number = 1;

    public Measure() {
    }

    public Measure(int number) {
        this.number = number;
    }

    //属性
    public Measure addAttributes(Attributes attributes) {
        this.nodeList.add(attributes);
        return this;
    }

    //音符
    public Measure addNote(Note note) {
        this.nodeList.add(note);
        return this;
    }

    //同步
    public Measure addBackup(Backup backup) {
        this.nodeList.add(backup);
        return this;
    }
}

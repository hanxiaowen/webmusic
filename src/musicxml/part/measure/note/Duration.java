package musicxml.part.measure.note;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

//持续时长
@XmlNode(name = "duration")
public class Duration extends Node {

    @XmlText
    public int value=0;

    public Duration() {
    }

    public Duration(int value) {
        this.value = value;
    }
}

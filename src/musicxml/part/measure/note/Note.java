package musicxml.part.measure.note;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.part.measure.note.pitch.Alter;
import musicxml.part.measure.note.pitch.Octave;
import musicxml.part.measure.note.pitch.Pitch;
import musicxml.part.measure.note.pitch.Step;

//音符
@XmlNode(name = "note")
public class Note extends Node {

    public Note addPitch(Step step, Alter alter, Octave octave) {
        Pitch pitch = new Pitch();
        pitch.addStep(step);
        pitch.addAlter(alter);
        pitch.addOctave(octave);
        this.nodeList.add(pitch);
        return this;
    }


    public Note addPitch(Pitch pitch) {
        this.nodeList.add(pitch);
        return this;
    }

    public Note addType(String type){
        Type type1=new Type(type);
        return  addType(type1);
    }
    public Note addType(Type type) {
        this.nodeList.add(type);
        return this;
    }


    public Note addStaff(int staff){
        Staff staff1=new Staff(staff);
        return addStaff(staff1);
    }

    public Note addStaff(Staff staff) {
        this.nodeList.add(staff);
        return this;
    }

    public Note addDuration(int duration) {
        Duration duration1 = new Duration(duration);
        return addDuration(duration1);
    }

    public Note addDuration(Duration duration) {
        this.nodeList.add(duration);
        return this;
    }

    //和弦标记
    public Note addChord(Chord chord) {
        this.nodeList.add(chord);
        return this;
    }

    //附点标记
    public Note addDot(Dot dot) {
        this.nodeList.add(dot);
        return this;
    }

    //倚音标记
    public Note addGrace(Grace grace) {
        this.nodeList.add(grace);
        return this;
    }


    //休止符标记
    public Note addRest(Rest rest) {
        this.nodeList.add(rest);
        return this;
    }

}

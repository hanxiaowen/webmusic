package musicxml.part.measure.note;

import musicxml.annotion.XmlNode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

//附点标记
@XmlNode(name = "dot")
public class Dot extends Note{
}

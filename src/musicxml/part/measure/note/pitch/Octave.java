package musicxml.part.measure.note.pitch;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

//八度
@XmlNode(name = "octave")
public class Octave extends Node {
    @XmlText
    public int value = 0;

    public Octave() {
    }

    public Octave(int value) {
        this.value = value;
    }
}

package musicxml.part.measure.note.pitch;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

//升降调
@XmlNode(name = "alter")
public class Alter extends Node {

    @XmlText
    public int value=0;

    public Alter() {
    }

    public Alter(int value) {
        this.value = value;
    }
}

package musicxml.part.measure.note.pitch;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

@XmlNode(name = "step", desc = "音名")
public class Step extends Node {
    @XmlText
    public String value = "";

    public static String C = "C";
    public static String D = "D";
    public static String E = "E";
    public static String F = "F";
    public static String G = "G";
    public static String A = "A";
    public static String B = "B";

    public Step(String value) {
        this.value = value;
    }
}

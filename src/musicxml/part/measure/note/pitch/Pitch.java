package musicxml.part.measure.note.pitch;

import musicxml.annotion.XmlNode;
import musicxml.part.measure.note.Note;

//音高
@XmlNode(name = "pitch")
public class Pitch extends Note {



    public Pitch() {
    }

    public Pitch(Step step, Octave octave, Alter alter) {
        addStep(step);
        addOctave(octave);
        addAlter(alter);
    }

    //音阶名字
    public Pitch addStep(Step step) {
        this.nodeList.add(step);
        return this;
    }

    //八度
    public Pitch addOctave(Octave octave) {
        this.nodeList.add(octave);
        return this;
    }

    //升降调
    public Pitch addAlter(Alter alter) {
        this.nodeList.add(alter);
        return this;
    }
}

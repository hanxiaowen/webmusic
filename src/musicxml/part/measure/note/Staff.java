package musicxml.part.measure.note;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

//谱位置编号
@XmlNode(name = "staff")
public class Staff extends Node {

    @XmlText
    public int staff = 1;

    public Staff() {
    }

    public Staff(int staff) {
        this.staff = staff;
    }
}

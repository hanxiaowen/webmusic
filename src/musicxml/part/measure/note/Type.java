package musicxml.part.measure.note;

import musicxml.Node;
import musicxml.annotion.XmlNode;
import musicxml.annotion.XmlText;

@XmlNode(name = "type")
public class Type  extends Node {
    @XmlText
    public String value="";

    public static String TYPE_256th = "256th";
    public static String TYPE_128th = "128th";
    public static String TYPE_64th = "64th";
    public static String TYPE_32nd = "32nd";
    public static String TYPE_16th = "16th";
    public static String TYPE_eighth = "eighth";
    public static String TYPE_quarter = "quarter";
    public static String TYPE_half = "half";
    public static String TYPE_whole = "whole";
    public static String TYPE_breve = "breve";
    public static String TYPE_long = "long";

    public Type() {
    }

    public Type(String value) {
        this.value = value;
    }
}

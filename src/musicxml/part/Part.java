package musicxml.part;

import musicxml.Node;
import musicxml.annotion.XmlAttribute;
import musicxml.annotion.XmlNode;
import musicxml.part.measure.Measure;

//声部信息
@XmlNode(name = "part", desc = "分谱")
public class Part extends Node {

    //编号
    @XmlAttribute
    public String id = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Part() {
    }

    public Part(String id) {
        this.id = id;
    }

    //小节
    public Part addMeasure(Measure measure) {
        this.nodeList.add(measure);
        return this;
    }

    //获取小节
    public Measure getMeasure(int number) {
        for (Node node : nodeList) {
            if (node instanceof Measure) {
                Measure m = (Measure) node;
                if (m.number == number) {
                    return m;
                }
            }
        }
        return null;
    }
}

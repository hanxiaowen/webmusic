### 可编程式音乐描述集合（computer programming of music description toolkit,mdt）
```
目录
1、musicxml数字乐谱描述
2、musiccode乐谱可编程性
3、player乐谱播放器
4、musicscore乐谱绘图
5、GUI用户界面
6、MIDI和其它
```

<img src="https://images.gitee.com/uploads/images/2021/0803/222512_1a8240df_617934.png" alt="图片替换文本" width="600" height="400" />

### 一、musicxml数字乐谱描述套件

在musicxml中，ScorePartwise是数字乐谱的顶层数据结构，其语法必须符合W3C规范：

```
package musicxml.example;

import musicxml.ScorePartwise;
import musicxml.part.Part;
import musicxml.part.measure.Measure;
import musicxml.part.measure.attributes.Attributes;
import musicxml.part.measure.attributes.Division;
import musicxml.part.measure.attributes.clef.Clef;
import musicxml.part.measure.attributes.key.Key;
import musicxml.part.measure.attributes.time.Time;
import musicxml.part.measure.note.Duration;
import musicxml.part.measure.note.Note;
import musicxml.part.measure.note.Staff;
import musicxml.part.measure.note.Type;
import musicxml.part.measure.note.pitch.Alter;
import musicxml.part.measure.note.pitch.Octave;
import musicxml.part.measure.note.pitch.Pitch;
import musicxml.part.measure.note.pitch.Step;

public class Music_1 {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        //创建乐谱
        ScorePartwise scorePartwise = new ScorePartwise("music1");

        //设置分谱和乐器名字
        scorePartwise.addScorePart("p1", "piano");

        //分谱
        Part part = scorePartwise.getPart("p1");

        //小节
        Measure measure = new Measure(1);
        Attributes attributes = new Attributes();
        //division要最先设置
        attributes.addDivision(Division.D256()).addKey(Key.MajorInC()).addTime(Time.Beat44());
        attributes.addStaves(2).addClef(Clef.G(1)).addClef(Clef.F(2));
        measure.addAttributes(attributes);
        part.addMeasure(measure);

        //音符
        Note note = new Note();
        note.addPitch(new Step("C"), new Alter(0), new Octave(4));
        note.addDuration(1024);
        note.addType(Type.TYPE_whole);
        note.addStaff(1);
        measure.addNote(note);

        //转换到xml格式
        String s = scorePartwise.toXML();
        scorePartwise.toFile("music1.xml");
        System.out.println(s);

    }
}
```
生成的音乐描述文件musicxml格式文件如下：
```
<score-partwise version='3'>                                                              <!-- 乐谱 -->
    <work>                                                                                <!-- 工作 -->
        <work-title>music1</work-title>                                                   <!-- 谱名 -->
    </work>
    <part-list>                                                                           <!-- 谱表 -->
        <score-part id='p1'>                                                              <!-- 声部 -->
            <part-name>piano</part-name>                                                  <!-- 名字 -->
        </score-part>
    </part-list>
    <part id='p1'>                                                                        <!-- 分谱 -->
        <measure number='1'>                                                              <!-- 小节 -->
            <attributes>                                                                  <!-- 属性 -->
                <divisions>256</divisions>                                                <!-- 时长 -->
                <key>                                                                     <!-- 调式 -->
                    <fifths>0</fifths>                                                    <!-- 五度 -->
                    <mode>major</mode>                                                    <!-- 类型 -->
                </key>
                <time>                                                                    <!-- 时间 -->
                    <beats>4</beats>                                                      <!-- 节奏 -->
                    <beat-type>4</beat-type>                                              <!-- 类型 -->
                </time>
                <staves>2</staves>                                                        <!-- 个数 -->
                <clef number='1'>                                                         <!-- 结点 -->
                    <sign>G</sign>                                                        <!-- 结点 -->
                    <line>2</line>                                                        <!-- 结点 -->
                </clef>
                <clef number='2'>                                                         <!-- 结点 -->
                    <sign>C</sign>                                                        <!-- 结点 -->
                    <line>4</line>                                                        <!-- 结点 -->
                </clef>
            </attributes>
            <note>                                                                        <!-- 结点 -->
                <pitch>                                                                   <!-- 结点 -->
                    <step>C</step>                                                        <!-- 音名 -->
                    <alter>0</alter>                                                      <!-- 结点 -->
                    <octave>4</octave>                                                    <!-- 结点 -->
                </pitch>
                <duration>1024</duration>                                                 <!-- 结点 -->
                <type>whole</type>                                                        <!-- 结点 -->
                <staff>1</staff>                                                          <!-- 结点 -->
            </note>
        </measure>
    </part>
</score-partwise>
```
用musescore软件渲染结果如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0814/232710_51c07128_617934.png "屏幕截图.png")

### 二、musiccode乐谱可编程性套件
```
# 可编程式音乐描述语言（computer programming of music description language,mdl）
work="测试"
part name=piano

    # 片段
    staff id=1

        c---                            # 一个全音符为一行
        d- d-                           # 二分音符
        d d d d                         # 四分音符
        -fd -fc -fs -sdf                # 八分音符
        --aaaaaaaa                      # 十六分音符
        ---bbbbbb                       # 三十二分音符
        [cdf] d [fewd] d d w d 1        # 和弦
        [---kdf]

        C                               #C和弦，等价于[135]
        Cm                              #小C和弦，等价于[1b35]


    end


    staff id=2
   

    end


end
```


三、player乐谱播放器套件

四、musicscore乐谱绘图套件

五、GUI套件

六、midi和其它套件